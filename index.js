
const express = require('express');
const app = express();
const cors = require('cors')

app.use(express.json())
app.use(express.static('assets'))

app.get('/', (req, res) => {
    return res.sendFile(__dirname + '/ori/recipient.html');
});

app.get('/init', (req, res) => {
    return res.sendFile(__dirname + '/ori/initiater.html');
});

let sdp;
app.post('/sdp', (req, res) => {
    sdp = req.body.sdp
    res.json({ status: 'ok' })
});
app.get('/sdp', cors(), (req, res) => {
    if (sdp)
        return res.json({ sdp })

    res.status(404).send('Not Ready')
});

//listen on the app
app.listen(3000, () => {
    return console.log('Server is up on 3000')
});
